using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Controllers.Resources;
using vega.Models;
using vega.Persistence;

namespace vega.Controllers
{
    public class MakesController : ControllerBase
    {
        private readonly VegaDbContext context;
        private readonly IMapper Mapper;
        public MakesController(VegaDbContext context, IMapper mapper)
        {
            this.context = context;

            this.Mapper = mapper;
        }

        [HttpGet("/api/makes")]
        public async Task<IEnumerable<MakeResource>> GetMakes()
        {
            var makes = await context.Makes.Include(m => m.Models).ToListAsync();

            return Mapper.Map<List<Make>, List<MakeResource>>(makes);
        }
    }
}
