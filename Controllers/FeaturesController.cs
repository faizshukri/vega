using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Controllers.Resources;
using vega.Models;
using vega.Persistence;

namespace vega.Controllers
{
    public class FeaturesController : ControllerBase
    {
        public readonly VegaDbContext context;
        public readonly IMapper mapper;
        public FeaturesController(VegaDbContext context, IMapper mapper)
        {
            this.context = context;

            this.mapper = mapper;

        }

        [HttpGet("/api/features")]
        public async Task<IEnumerable<FeatureResource>> GetFeatures()
        {
            var makes = await context.Features.ToListAsync();

            return Mapper.Map<List<Feature>, List<FeatureResource>>(makes);
        }
    }
}